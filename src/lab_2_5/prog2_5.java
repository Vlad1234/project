package lab_2_5;

import static java.lang.System.out;

abstract class Car
{
	//instance field
	String brand;
	String classOf;
	int weight;
	Engine motor;

	//constructors
	Car()
	{

	}

	//methods
	public abstract void start();
	public abstract void stop();
	public abstract void printInfo();

	public void turnRight()
	{
		out.println("turning right");
	}

	public void turnLeft()
	{
		out.println("turning left");
	}

}

abstract class Engine
{
	//instance field
	String power;
	String productor;

	//constructors
	Engine()
	{

	}

	//methods

}

class Lorry extends Car
{
	//instance field
	int maxWeight;

	//constructors
	Lorry(String brand, int weight, int maxWeight)
	{
		this.brand = brand;
		this.classOf = "truck";
		this.weight = weight;
		this.maxWeight = maxWeight;
	}

	//methods
	public void start()
	{
		out.println("truck started move");
	}

	public void stop()
	{
		out.println("truck stopped");
	}

	public void printInfo()
	{
		out.println("brand is: " + this.brand + "\n class is: " + this.classOf + "\n weight is: " + this.weight + "\n maximal weight is: " + this.maxWeight);
	}

}

class SportCar extends Car
{
	//instance field
	int maxSpeed;

	//constructors
		SportCar(String brand, int weight, int maxSpeed)
	{
		this.brand = brand;
		this.classOf = "sport";
		this.weight = weight;
		this.maxSpeed = maxSpeed;
	}

	//methods
	public void start()
	{
		out.println("sportcar started move");
	}

	public void stop()
	{
		out.println("sportcar stopped");
	}

	public void printInfo()
	{
		out.println("brand is: " + this.brand + "\n class is: " + this.classOf + "\n weight is: " + this.weight + "\n maximal speed is: " + this.maxSpeed);
	}

}

public class prog2_5
{
	public static void main(String args[])
	{
		Lorry newTruck = new Lorry("Volvo", 15000, 10000);
		SportCar newFastCar = new SportCar("Lamborghini", 1000, 390);

		newTruck.printInfo();
		newFastCar.printInfo();
	}
}
