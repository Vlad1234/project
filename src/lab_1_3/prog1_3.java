package lab_1_3;

import java.util.Scanner;

public class prog1_3
{

	public static int read()
	{
		int variable = 0;

		Scanner in = new Scanner(System.in);
		variable = in.nextInt();
		in.close();

		return variable;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

        public static void textln()//print in console
        {
                System.out.print("");
        }

	public static int isPositiveOrNegative(int number)
	{

		int number1 = number;

		if(number1 > 0)
			number1++;

		return number1;
	}

        public static void main(String args[])
	{
		int number = 0;

		text("enter number: ");
		number = read();
		number = isPositiveOrNegative(number);
		text("Your number is:" + number);

	}
}
