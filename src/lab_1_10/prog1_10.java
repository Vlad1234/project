package lab_1_10;

public class prog1_10
{
	public static void replace(int[] array1, int number1, int[] array2, int number2)//replace elements of arrays
	{
			array1[number1] += array2[number2];
			array2[number2] = array1[number1] - array2[number2];
			array1[number1] = array1[number1] - array2[number2];
	}

        public static void arrayOut(int[] array1)//output array
        {
       		for(int i = 0; i < array1.length; i++)
		{
                	System.out.print(array1[i]);
			System.out.print("  ");
		}

        }

 	public static void sort(int[] userArraySort)//sort function
        {
		int rightBoundary = userArraySort.length;
		boolean isChange = true;

		while((rightBoundary > 1) && (isChange == true))
		{
			isChange = false;
			for(int i = 0;i < rightBoundary - 1; i++)
			{
				if((userArraySort[i] == 0) && (userArraySort[i + 1] != 0))
				{
					replace(userArraySort, i, userArraySort, i+1);
					System.out.print("Changing [");
					System.out.print(userArraySort[i]);
					System.out.print(", ");
					System.out.print(userArraySort[i + 1]);
					System.out.print("]");
					isChange = true;
				}
				
			}
			System.out.println();
			System.out.print("-->  ");
			arrayOut(userArraySort);
			System.out.print("<--");
			rightBoundary--;
		}
	}

	public static void main(String[] args)//main function
	{
		int[] userArray= {15, 10, 0, -6, -5, 3, 0, -34, 0, 32, 56, 0, 24, -52};
		arrayOut(userArray);
		sort(userArray);
		System.out.println();
		arrayOut(userArray);
	}
}
